# Terminal Slots

## Description
This is a text based Slot Machine that runs in your terminal. It will take a deposit amount as input that will allow you to play on one of three lines. If any applicable lines win the bet will be multiplied by that line and added to your balance. You will be able to play until your balance runs out or until you want to cash out.

Feel free to copy a the repository.
Run the main.py file and follow instructions.
